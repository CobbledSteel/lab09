/******************************************************************************
* Assignment:  lab09
* Lab Section: 11/4/2015  15:30 SC189
* Description: Prompts the user for two positive integers. Given the two 
*              integers, prints whether or not they are the same, have one 
*              different digit, or, if those former conditions are not met, 
*              it prints the smallest number larger than the smaller number
*              that has at most one differing digit from the larger one.
* Programmers: Vadim Nikiforov vnikifor@purdue.edu
*              Ziheng wang wang2194@purdue.edu
*              Haozheng Qu qu34@purdue.edu
******************************************************************************/
#include <stdio.h>
#include <math.h>

// function declarations
int getValue(int);
int numDifferent(int, int);
int getAnswer(int, int);
void printAnswer(int, int);
int numDigits(int);
int getSmaller(int, int);
int getBigger(int, int);

int main(void)
{
  // local delcarations
  int number1; // the first number the user enters
  int number2; // the second number the user numbers

  // retrieve data from the user
  number1 = getValue(1);
  number2 = getValue(2);

  // print the results of the algorithm
  printAnswer(number1, number2);
  return 0;
}

/******************************************************************************
* Function:    numDigits
* Description: find the number of the digits passed to the function
* Parameters:  num, int, the number of which to find the number of digits
* Return:      int, the number of digits in a number
******************************************************************************/
int numDigits(int num)
{
  // local declarations
  int answer; // the number of digits in a number

  answer = (int) (log10(num) + 1); // Use log function to get the number of digits

  return answer;
}

/******************************************************************************
* Function:    getSmaller
* Description: Compare two values and return the smaller one
* Parameters:  num1, int, the first value
*              num2, int, the second value
* Return:      int, the smaller of the two numbers
******************************************************************************/
int getSmaller(int num1, int num2)
{
  // local declarations
  int smaller; // the smaller of the two numbers
  smaller = num1 < num2 ? num1 : num2;

  return smaller;
}

/******************************************************************************
* Function:    getBigger
* Description: Compare two values and return the bigger one
* Parameters:  num1, int, the first value
*              num2, int, the second value
* Return:      int, the bigger of the two numbers
******************************************************************************/
int getBigger(int num1, int num2)
{
  // local declarations
  int bigger; // the bigger of the two numbers
  bigger = num1 > num2 ? num1 : num2;

  return bigger;
}

/******************************************************************************
* Function:    getValue
* Description: get a positive integer from the user, notifying the user if they
*              input a zero or negative number, and prompting again
* Parameters:  numberAsked, int, the number of times the user has been prompted
*              so far
* Return:      int, the number the user has entered
******************************************************************************/
int getValue(int numberAsked)
{
  // local declarations
  int userAnswer; // the value the user enters

  printf("Enter value #%d: ", numberAsked);
  scanf("%d", &userAnswer);
  // if the user enters a negative or zero number, prompt them until they enter a positive number
  while(userAnswer < 0)
  {
    printf("\nError! Non-negative values only!!\n\nEnter value:#%d: ",numberAsked);//printf error message and get new input
    scanf("%d", &userAnswer);
  }

  return userAnswer;
}

/******************************************************************************
* Function:    numDifferent
* Description: find the number of different digits between two numbers
* Parameters:  num1, int, the first number to compare the digits of
*              num1, int, the second number to compare the digits of
* Return:      int, the number of different digits between the two numbers
******************************************************************************/
int numDifferent(int num1, int num2)
{
  // local declarations
  int bigger; // the bigger of the two numbers
  int numberDigits; // the number of digits in the bigger number
  int i; // a loop counter
  int numDifferent; // the number of digits different between the two numbers

  bigger = getBigger(num1, num2);
  numberDigits = numDigits(bigger); 
  numDifferent = 0;

  // compare each digit one by one
  for(i=0; i<numberDigits; i++)
  {
    numDifferent += num1 % 10 != num2 % 10; // if the digits are different, add one
    num1 /= 10;
    num2 /= 10; //do the next digit
  }

  return numDifferent;
}

/******************************************************************************
* Function:    getAnswer
* Description: given two numbers, returns the smallest number larger than the 
*              smaller number that has at most one digit that is different from
*              the larger number
* Parameters:  num1, int, the first number to be used in the algorithm
*              num2, int, the second number to be used in the algorithm
* Return:      int, the number that is the solution given by the algorithm
******************************************************************************/
int getAnswer(int num1, int num2)
{
  // local declarations
  int bigger; // the bigger of the two numbers
  int smaller; // the smaller of the two numbers
  int different; // how many digits are different between the two numbers
  int smallerFixed; // a fixed value of the smaller number for use in an edge-case
  int i; // a loop counter
  int numberDigits; // the number of digits in the bigger number
  int answer; // the number that's the solution to the algorithm

  bigger = getBigger(num1, num2);
  smaller = getSmaller(num1, num2);
  smallerFixed = smaller; // need this because smaller gets decreased in the loop, and saving the smaller number is needed for an edge-case
  different = numDifferent(num1, num2);
  numberDigits = numDigits(bigger);
  answer = 0;
  // for each digit starting from the ones digit, start changing digits to the upper numbers' digit
  for(i=0; i<numberDigits; i++)
  {
    // if a digit is different between the two numbers, make the result's digit the larger number's digit if there are more than 1 differing digits left
    if(different > 1 && smaller % 10 != bigger % 10)
    {
      // add the digit to the result
      answer += pow(10,i) * (bigger % 10);
      different--;
    }
    // deal with an edge case in this algorithm
    else if(numberDigits - i == 1)
      answer += pow(10, i) * (answer < smallerFixed); // if the answer is bigger than the smaller number, add a zero digit, but if it is smaller, do the next best option which is adding a one
    // if there is only one digit different, use values from the smaller number
    else
      answer += pow(10, i) * (smaller % 10);
    bigger /= 10; // move on to the next digit of the bigger number
    smaller /= 10; // move on to the next digit of the smaller number
  }
  
  return answer;
}

/******************************************************************************
* Function:    printAnswer
* Description: prints whether or not the two numbers passed to the function are
*              the same, have one digit different, or prints the smallest 
*              number greater than the smaller number of the two that has at 
*              most one digit different from the larger one
* Parameters:  num1, int, the first number to use in the algorithm
*              num2, int, the second number to be used in the algorithm
* Return:      void
******************************************************************************/
void printAnswer(int num1, int num2)
{
  // local declarations
  int bigger; // the bigger of the two numbers
  int smaller; // the smaller of the two numbers
  int answer; // the result of the algorithm
  int numberDigits; // the number of digits of the larger number

  // if the two numbers are the same, say so
  if(numDifferent(num1, num2) == 0)
    printf("\nThe two numbers are the same.\n");
  // if the numbers have one digit different, say so
  else if(numDifferent(num1, num2) == 1)
    printf("\nThe two numbers have exactly one different digit.\n");
  // if the numbers have more than one digit different, print the solution to the algorithm.
  else
  {
    bigger =  getBigger(num1, num2);
    smaller = getSmaller(num1, num2);
    numberDigits = numDigits(bigger);
    answer = getAnswer(num1, num2);

    printf("\nThe two numbers have more than one different digit.\n\n");
    printf("The next value larger than %d that has one digit different from %d is: %0*d\n", smaller, bigger, numberDigits, answer); // print the answer, using %0*d to put the proper number of zeros
  }

  return;
}
